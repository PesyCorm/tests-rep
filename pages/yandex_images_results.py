from framework import *


class YandexImagesResults(Page):
    """Страница с результатами поиска по картинкам в Яндекс"""

    search_fld = Element(By.CSS_SELECTOR, '.search2__input input', 'Поиск')
    images_lst = ElementsList(By.CSS_SELECTOR, '.serp-item', 'Картинки')

    def check_load(self):
        """Проверяем загрузку"""

        self.search_fld.wait(Displayed)
        self.images_lst.wait(Displayed)

    def check_search_text(self, expected_text):
        """Проверяем текст в строке поиска
        :param expected_text: эталонный текст
        """
        self.search_fld.wait(ContainsText(expected_text))

    def open_image(self, num):
        """Открываем изображение
        :param num: номер изображения
        """
        self.images_lst.item(num).click()


@set_parent('.ImagesViewer-Container')
class ImageContainer(Page):
    """Открытое изображение"""

    image = Element(By.CSS_SELECTOR, '.MMImageContainer', 'Изображение')
    button_right = Element(By.CSS_SELECTOR, '.CircleButton_type_next', 'Кнопка перелистывания вправо')
    button_left = Element(By.CSS_SELECTOR, '.CircleButton_type_prev', 'Кнопка перелистывания влево')

    def check_open(self):
        """Проверяем что открылось"""
        self.image.wait(Displayed)

    def scroll_image(self, next=True):
        """Перелистываем изображение
        :param next: следующее или листаем назад
        """
        if next:
            self.button_right.click()
        else:
            self.button_left.click()