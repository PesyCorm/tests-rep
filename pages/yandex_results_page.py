from framework import *


@page_name('Результаты поиска')
class YandexResultsPage(Page):
    """Страница с результатами поиска"""

    search_fld = Element(By.CSS_SELECTOR, '.search2__input', 'Поле поиска')
    result_titles = ElementsList(By.CSS_SELECTOR, '.organic__title', 'Заголовки результатов поиска')
    result_links = ElementsList(By.CSS_SELECTOR, '.Organic-Path', 'Ссылки результатов поиска')

    def check_load(self):
        """Проверяем загрузку"""
        self.search_fld.wait(Displayed)
        self.result_titles.wait(Displayed)

    def check_result_links(self, *values):
        """Проверяем ссылки результатов поиска, в порядке сверху-вниз
        :param values: значения ссылок
        """
        for num in range(len(values)):
            self.result_links.item(num).wait(ContainsText(values[num]))