from framework import *


@page_name('Главная')
@set_parent('.body__wrapper')
class YandexMain(Page):
    """Главная страница yandex.ru"""

    search_fld = Element(By.CSS_SELECTOR, '.search2__input input', 'Поле поиска')
    search_btn = Element(By.CSS_SELECTOR, '.search2__button', 'Кнопка Поиск')
    services_lst = ElementsList(By.CSS_SELECTOR, '.services-new__item', 'Сервисы')
    suggest_lst = ElementsList(By.CSS_SELECTOR, '.mini-suggest__item', 'Подсказки поля поиска', page_parent=False)

    def open(self, check_load=True):
        """Открываем страницу"""
        self.browser.open('yandex.ru')
        if check_load:
            self.check_load()

    def check_load(self):
        """Проверяем загрузку"""
        self.search_fld.wait(Displayed)
        self.services_lst.wait(Displayed)

    def search(self, text, search_btn_click=True):
        """Поиск
        :param text: текст который вводим в поле поиска
        :param search_btn_click: кликнуть кнопку Поиск
        """
        self.search_fld.type_in(text)
        if search_btn_click:
            self.go_search()

    def check_suggest(self, displayed=True):
        """Проверяем наличие подсказки поиска
        :param displayed: отображается
        """
        if displayed:
            self.suggest_lst.wait(Displayed)
        else:
            self.suggest_lst.wait_not(Displayed)

    def go_search(self, by_btn=True):
        """Кликаем Поиск
        :param by_btn: клик по кнопке поиска, иначе отправляем в поле поиска Enter
        """
        if by_btn:
            self.search_btn.click()
        else:
            self.search_fld.type_in(Keys.ENTER)

    def select_service(self, service):
        """Переходим в сервис Яндекса
        :param service: сервис
        """
        self.services_lst.item(contains_text=service).click()