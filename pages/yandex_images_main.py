from framework import *


@set_parent('.b-page')
class YandexImagesMain(Page):
    """Главная страница Яндекс.Картинки"""

    search_fld = Element(By.CSS_SELECTOR, '.search2__input input', 'Поиск')
    blocks_lst = ElementsList(By.CSS_SELECTOR, '.PopularRequestList-Item', 'Предложенные категории картинок')

    def check_load(self):
        """Проверяем загрузку"""
        self.search_fld.wait(Displayed)
        self.blocks_lst.wait(Displayed)

    def get_block_text(self, num):
        """Получаем текст из категории картинок
        :param num: номер категории
        """
        return self.blocks_lst.item(num).wait(Displayed).text

    def open_block(self, num=None, text=None):
        """Открываем предложенную категорию картинок
        :param num: номер категории
            или
        :param text: текст в категории
        """
        self.blocks_lst.item(num, text).click()