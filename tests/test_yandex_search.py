from framework import *

from pages.yandex_main import YandexMain
from pages.yandex_results_page import YandexResultsPage

import time


class TestYandexMainSearch(BaseTest):

    search_txt = 'Тензор'

    yandex_main = YandexMain()
    yandex_result_page = YandexResultsPage()

    def test_01_yandex_main_search(self):
        """Поиск на главной странице Яндекс"""

        self.yandex_main.open(check_load=True)

        log('Проверяем, что при вводе в поле поиска, появляется подсказка')
        self.yandex_main.search('Тензор', search_btn_click=False)
        self.yandex_main.check_suggest()
        self.yandex_main.go_search()

        self.yandex_result_page.check_load()

        log('Проверяем, что первые пять результатов поиска содержат ссылку на tensor.ru')
        self.yandex_result_page.check_result_links(*('tensor.ru',)*5)

    def test_02_check_assert(self):

        log('Второй тест из тестов по поиску погнал')
        self.yandex_main.open(check_load=True)
        time.sleep(3)


if __name__ == '__main__':
    run_test_file(__file__)