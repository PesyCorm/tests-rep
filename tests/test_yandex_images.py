from framework import *

from pages.yandex_main import YandexMain
from pages.yandex_images_main import YandexImagesMain
from pages.yandex_images_results import YandexImagesResults, ImageContainer


class TestYandexImages(BaseTest):

    yandex_main = YandexMain()
    yandex_images_main = YandexImagesMain()
    yandex_images_result = YandexImagesResults()
    image_container = ImageContainer()

    def test_01_yandex_images(self):

        log('Переходим в Яндекс Картинки')
        self.yandex_main.open()
        self.yandex_main.select_service('Картинки')
        self.browser.switch_to_opened_window()
        self.browser.wait(UrlContains('https://yandex.ru/images/'))
        self.yandex_images_main.check_load()

        log('Открываем первый блок с изображениями')
        self.yandex_images_main.open_block(0)
        self.yandex_images_result.check_load()

        log('Открываем первое изображение')
        self.yandex_images_result.open_image(0)
        self.image_container.check_open()

        log('Проверяем, что при перелистывании изображение меняется')
        image1 = self.browser.save_screen()
        self.browser.check_page_change(self.image_container.scroll_image)
        image2 = self.browser.save_screen()
        compare_screens(image1, image2, equal=False)
        self.image_container.scroll_image(next=False)


if __name__ == '__main__':
    run_test_file(__file__)